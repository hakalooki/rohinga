/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.5-10.1.36-MariaDB : Database - rohinga
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `rohingas` */

DROP TABLE IF EXISTS `rohingas`;

CREATE TABLE `rohingas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(17) NOT NULL,
  `creation_date` varchar(50) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `date_of_entry` varchar(50) NOT NULL,
  `machine_id` varchar(255) DEFAULT NULL,
  `name_eng` varchar(255) DEFAULT NULL,
  `father_name_eng` varchar(255) DEFAULT NULL,
  `mother_name_eng` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(50) NOT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `religion` varchar(11) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL,
  `police_station` varchar(255) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reference_no` (`reference_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `rohingas` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
