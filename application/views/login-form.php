<!DOCTYPE html>
<html lang="en">

<?php
if (isset($this->session->userdata['logged_in'])) {
	//header("location: http://localhost/rng/index.php/auth/user_login");
}
?>

<head>
	<title>Login Form</title>
	<link href = "<?php echo base_url();?>assets/css/bulma.min.css" rel = "stylesheet">
	
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	
</head>
<body>
	<?php
	if (isset($logout_message)) {
	echo "<div class='message'>";
	echo $logout_message;
	echo "</div>";
	}
	?>

	<section class="hero is-large">
	  <div class="hero-head">
	    <nav class="navbar">
	      <div class="container">
	        <div class="navbar-brand">

	          <a class="navbar-item">
	            <img class="image is-64x64" src="<?php echo base_url();?>assets/img/logo.png" width="" height="">
	          </a>
	          
	        </div>
	      </div>
	    </nav>
	  </div>

	  <div class="hero-body">
	  	<div class="columns  is-vcentered">
				<div class="column is-4 is-offset-4">
	        <form action="<?php echo base_url();?>index.php/auth/user_login" method="post" id="login_form">
	        	<?php
						echo "<div class='error_msg'>";
						if (isset($error_message)) {
							echo $error_message;
						}
						//echo validation_errors();
						echo "</div>";
						?>

	          <div class="field">
	            <div class="control has-icon">
	              <input class="input is-medium" type="text" name="username" placeholder="User Name">
	              <span class="icon">
								  <i class="fas fa-user"></i>
								</span>
	            </div>
	          </div>
	          <div class="field">
	            <div class="control has-icon">
	              <input class="input is-medium" type="password" name="password" placeholder="Password">
	              <span class="icon">
								  <i class="fas fa-key"></i>
								</span>
	            </div>
	          </div>
	          

	          <div class="field">
	            <div class="control">
	              <button type="submit" class="button is-medium is-fullwidth is-primary">
	                <span class="icon">
									  <i class="fas fa-unlock"></i>
									</span> <span>Log in</span>
	              </button>
	            </div>
	          </div>

	        </form>

      	</div>
        
	    	</div>
	  </div>

	  <div class="hero-foot">
	  	<p class="has-text-danger has-text-centered is-size-7">
				&copy; বাংলাদেশ নির্বাচন কমিশন
			</p>
	  </div>
	</section>


</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){

		/*$('#import_form').on('submit', function(event){
			
		});*/
		

	});
</script>