<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Import {

	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->helper('app');
		$this->CI->load->helper('directory');
		$this->CI->load->library('session');

		$this->CI->db->save_queries = false;
	}

  function insert_images_dir_single()
	{

		$fp = $this->CI->input->post('folder');

		$cnt = 0;
		$dp = 0;
		$data = array();

		$map = directory_map($fp);

		foreach ($map as $sf => $files) {
			$sfn = strstr($sf, '\\', TRUE);
			$exists = redundancy_check('biometric','pin',$sfn);

			if(!$exists) {
				$d = prepare_data($fp, $sf, $files);

				$this->CI->db->set($d);
				$this->CI->db->insert($this->CI->db->dbprefix . 'biometric');

				$cnt++;
			} else {
				$dp++;
			}
		}

		$sess_data = array(
				'cnt' => $cnt,
				'dp' => $dp,
				'status' => 'success'
				);
		$this->CI->session->set_userdata('import', $sess_data);

	}

	function insert_images_dir_batch()
		{

			$fp = $this->CI->input->post('folder');

			$cnt = 0;
			$dp = 0;

			$map = directory_map($fp);

			$_maps = array_chunk($map, 1200, true);

			foreach ($_maps as $datax) {
				$rs = array();

				foreach ($datax as $sf => $files) {

					$sfn = strstr($sf, '\\', TRUE);
					$exists = redundancy_check('biometric','pin',$sfn);

					if(!$exists) {
						$d = prepare_data($fp, $sf, $files);
						array_push($rs, $d);

						$cnt++;
					} else {
						$dp++;
					}
				}

				$this->CI->db->trans_start();

				$_datas = array_chunk($rs, 600);

				foreach ($_datas as $key => $data) {
					$this->CI->db->insert_batch('biometric', $data);
				}

				$this->CI->db->trans_complete();

			}

			$sess_data = array(
					'cnt' => $cnt,
					'dp' => $dp,
					'status' => 'success'
					);
			$this->CI->session->set_userdata('import', $sess_data);
		}

		function insert_images_dir_batch_all()
		{

			$fp = $this->CI->input->post('folder');

			$cnt = 0;
			$dp = 0;
			$rs = array();

			$map = directory_map($fp);

			foreach ($map as $sf => $files) {
				$sfn = strstr($sf, '\\', TRUE);
				$exists = redundancy_check('biometric','pin',$sfn);

				if(!$exists) {
					$d = prepare_data($fp, $sf, $files);
					array_push($rs, $d);

					$cnt++;
				} else {
					$dp++;
				}
			}

			$this->CI->db->trans_start();

			$_datas = array_chunk($rs, 100);

			foreach ($_datas as $key => $data) {
				$this->CI->db->insert_batch('biometric', $data);
			}

			$this->CI->db->trans_complete();

			$sess_data = array(
					'cnt' => $cnt,
					'dp' => $dp,
					'status' => 'success'
					);
			$this->CI->session->set_userdata('import', $sess_data);

		}

	function import_xml_single()
	{

		$fp = $this->CI->input->post('folder');

		$cnt = 0;
		$dp = 0;
		$data = array();

		$map = directory_map($fp);

		foreach ($map as $sf => $files) {
			$sfn = strstr($sf, '\\', TRUE);
			$exists = redundancy_check('voter','form_no',$sfn);

			if(!$exists) {
				$d = prepare_xml($fp, $sf, $files);

				$this->CI->db->set($d);
				$this->CI->db->insert($this->CI->db->dbprefix . 'voter');

				$cnt++;
			} else {
				$dp++;
			}
		}

		$sess_data = array(
				'cnt' => $cnt,
				'dp' => $dp,
				'status' => 'success'
				);
		$this->CI->session->set_userdata('importx', $sess_data);

	}


	function import_files_single()
	{

		$fp = $this->CI->input->post('folder');

		$cnt = 0;
		$dp = 0;
		$data = array();

		$map = directory_map($fp);

		if(checkd($map) == false) {
			unset($_SESSION['import']);
			return false;
		}

		foreach ($map as $sf => $files) {
			$sfn = strstr($sf, '\\', TRUE);
			$exists = redundancy_check('voter','form_no',$sfn);

			if(!$exists) {
				$id = generate_random_id();
				$dx = prepare_xml($fp, $sf, $files, $id);
				$db = prepare_data($fp, $sf, $files, $id);

				$this->CI->db->trans_start();

				$this->CI->db->set($dx);
				$this->CI->db->insert($this->CI->db->dbprefix . 'voter');

				$this->CI->db->set($db);
				$this->CI->db->insert($this->CI->db->dbprefix . 'biometric');

				$this->CI->db->trans_complete();

				$cnt++;
			} else {
				$dp++;
			}
		}

		$sess_data = array(
				'cnt' => $cnt,
				'dp' => $dp,
				'status' => 'success'
				);
		$this->CI->session->set_userdata('import', $sess_data);

	}
}