<!DOCTYPE html>
<html lang="en">

<?php
if (isset($this->session->userdata['logged_in'])) {
	$username = ($this->session->userdata['logged_in']['username']);
	$email = ($this->session->userdata['logged_in']['email']);
} else {
	header("location: http://localhost/rhng/index.php/auth/user_login");
}
?>

<head>
	<title>Import XML Form</title>
	<link href = "<?php echo base_url();?>assets/css/bulma.min.css" rel = "stylesheet">
	<link href = "<?php echo base_url();?>assets/css/style.css" rel = "stylesheet">
	<script src = "<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>
<body>
	<section class="hero is-large">
	  <div class="hero-head">
	    <nav class="navbar">
	      <div class="container">
	        <div class="navbar-brand">

	          <a class="navbar-item">
	            <img class="image is-64x64" src="<?php echo base_url();?>assets/img/ec_logo.png" width="" height="">
	          </a>
	          
	        </div>


	        <div id="navbarMenuHeroB" class="navbar-menu">
	          <div class="navbar-end">
	            <div class="navbar-itemxx">
	             <?php
	             	echo '<span class="has-text-bold has-text-success is-size-5">'.$this->session->userdata['logged_in']['username'].'</span>';
	             	echo '<br />';
	             	echo '<p class="has-text-grey has-text-centered is-size-6">'.$this->session->userdata['logged_in']['email'].'</p>';

	              ?>
	            </div>

	            <div class="navbar-item"></div>
	            <div class="navbar-item"></div>

	            <span class="navbar-item">
	              <a class="button is-succes is-inverted" href="<?php echo base_url();?>index.php/images/images_dir">
	                <span class="icon">
	                  <i class="fas fa-fingerprint"></i>
	                </span>
	                <span>Biometric</span>
	              </a>
	            </span>
	            
	            <span class="navbar-item">
	              <a class="button is-danger is-inverted" href="<?php echo base_url();?>index.php/auth/logout">
	                <span class="icon">
	                  <i class="fab fa-github"></i>
	                </span>
	                <span>Logout</span>
	              </a>
	            </span>
	          </div>
        	</div>

	      </div>
	    </nav>
	  </div>

	  <div class="hero-body">

	    <div class="container has-text-centered">
	    	<form action="<?php echo base_url();?>index.php/xml/import_xml" method="post" id="xml_form">
		      <div class="field has-addons has-addons-centered">
					  <div class="control">
					    <input class="input" type="text" name="folder" id="folder" placeholder="Enter a Directory Path">
					  </div>
					  <div class="control">
					  	<!--span>
								<a id="load" class="button is-success is-loading" hidden>Loading</a>
							</span-->
					    <button class="button is-success" type = "submit" name="import" id="import">
						    <span class="icon has-text-danger" id="load">
								  <i class="fas fa-pulse"></i>
								</span>
								<span>Import</span>
							</button>
					  </div>
					</div>

					<!--div class="columns is-centered">
						<div class="loader">importing....</div>
					</div-->

				</form>
				

				<?php 

				if (isset($this->session->userdata['importx'])) {
					if($this->session->userdata['importx']['cnt'] > 0)
						echo '<p id="cnt" class="has-text-success has-text-centered is-size-5">'.$this->session->userdata['importx']['cnt']. ' XML data imported</p>';

					if ($this->session->userdata['importx']['dp'] > 0)
						echo '<p id="dp" class="has-text-danger has-text-centered is-size-5">'.$this->session->userdata['importx']['dp']. ' XML data already exits</p>';
				}

				?>

	    </div>

	    <br/><br/>

	    <div class="columns is-centered">
				<div class="loader"></div>
			</div>

	  </div>

	  <div class="hero-foot">
	  	<p class="has-text-danger has-text-centered is-size-7">
				&copy; বাংলাদেশ নির্বাচন কমিশন
			</p>
	  </div>
	</section>

</body>
</html>









































<script type="text/javascript">
	$(document).ready(function(){
		$('#load').hide();
		$('.loader').hide();
		//$('#cnt').hide();
		//$('#dp').hide();

		$('#xml_form').on('submit', function(event){
			$('#load').show();
			$('.loader').show();
			$('#cnt').hide();
			$('#dp').hide();
		});
		

	});
</script>