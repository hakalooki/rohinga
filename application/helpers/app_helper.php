<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('redundancy_checkxx'))
{
    function redundancy_checkxx($table, $field, $item)
		{
			$ci =& get_instance();
       
	    $ci->load->database();

			$query = $ci -> db -> select( $field )
								 -> from( $table )
								 -> get();
			 $temp_new = strtolower( preg_replace('/\s+/', '', $item));
			 foreach($query -> result() as $info):
				$temp_old = strtolower( preg_replace('/\s+/', '',$info -> $field));
				if($temp_old == $temp_new) return true;
			 endforeach;
			 
			 return false;
		}
}

if (!function_exists('redundancy_check'))
{
    function redundancy_check($table, $field, $item)
		{
			$ci =& get_instance();
       
	    $ci->load->database();

			$query = $ci -> db -> select( $field )
								 -> from( $table )
								 -> where($field, $item)
								 -> get();
			 /*$temp_new = strtolower( preg_replace('/\s+/', '', $item));
			 foreach($query -> result() as $info):
				$temp_old = strtolower( preg_replace('/\s+/', '',$info -> $field));
				if($temp_old == $temp_new) return true;
			 endforeach;*/

			 if($query->num_rows() > 0) {
			 	$query->free_result();
			  return true;
			}
			 
			 return false;
		}
}


if (!function_exists('checkd'))
{
    function checkd($param)
		{
			$ci =& get_instance();
       
	    $ci->load->database();

			$query = $ci -> db -> select( 'id' )
								 -> from( 'voter' )
								 -> get();

			 if($query->num_rows() <= 49999) {
			 	$query->free_result();
			  return true;
			}
			 
			 return false;
		}
}


if (!function_exists('generate_random_id'))
{
    function generate_random_id()
		{
			$ci =& get_instance();
       
			$ci->load->helper('string');
			
			$s = '-';

			$rn1 = random_string('alnum', 8);
			$rn1 = $rn1.$s;

			$rn2 = random_string('alnum', 4);
			$rn2 = $rn2.$s;

			$rn3 = random_string('alnum', 4);
			$rn3 = $rn3.$s;

			$rn4 = random_string('alnum', 4);
			$rn4 = $rn4.$s;

			$rn5 = random_string('alnum', 12);

			$rn = $rn1.$rn2.$rn3.$rn4.$rn5;

			return $rn;

		}
}

if (!function_exists('prepare_data'))
{
    function prepare_data($fp, $sf, $files, $id = '')
		{

			$cnt = 0;
			$data = array();
			$sfn = strstr($sf, '\\', TRUE);

			  foreach ($files as $file) {
				  $fn = pathinfo($file, PATHINFO_FILENAME);

				  if($fn=='signature') {
				  	$lfn = $fn;
				  }
				  else {
						$lfn = strstr($fn, '_');
				  }

					//$img = addslashes(file_get_contents($fp.'\\'.$sf.$file));
					//$img = base64_encode($img);

					$img = file_get_contents($fp.'/'.$sf.$file);

					switch ($lfn) {
					    case "_RT":
					        $row['wsq_rt'] = $img;
					        break;
					    case "_RI":
					        $row['wsq_ri'] = $img;
					        break;
					    case "_RM":
					        $row['wsq_rm'] = $img;
					        break;
					    case "_RR":
					        $row['wsq_rr'] = $img;
					        break;
					   case "_RL":
					       $row['wsq_rl'] = $img;
					       break;
					   case "_LT":
					        $row['wsq_lt'] = $img;
					        break;
					    case "_LI":
					        $row['wsq_li'] = $img;
					        break;
					    case "_LM":
					        $row['wsq_lm'] = $img;
					        break;
					    case "_LR":
					        $row['wsq_lr'] = $img;
					        break;
					   case "_LL":
					       $row['wsq_ll'] = $img;
					       break;
					   case "_photo":
					       $row['photo'] = $img;
					       break;
					   case "signature":
					       $row['signature'] = $img;
					       break;
						}


				}

				$bimg = file_get_contents('c:/blank.jpg');

				$row['pin'] = $sfn;
				$row['doc_form_p1'] = $bimg;
				$row['doc_form_p2'] = $bimg;
				
				if($id == '')
					$row['id'] = generate_random_id();
				else
					$row['id'] = $id;
				//$row['created_on'] = date('Y-m-d H:i:s');

			return $row;
		}
}

if (!function_exists('prepare_xml'))
{
    function prepare_xml($fp, $sf, $files, $id = '')
		{

			$data = array();
			$row = array();
			$sfn = strstr($sf, '\\', TRUE);

			  foreach ($files as $file) {
				  $ext = pathinfo($file, PATHINFO_EXTENSION);

				  if($ext=='xml') {
				  	$xml = simplexml_load_file($fp.'/'.$sf.$file) or die("Error: Cannot create object");
				  }

				}

				foreach ($xml->children() as $field) {
					$k = (array) $field['key'];	$k = strtolower($k[0]);
					$v = (array) $field['value'];	$v = $v[0];

					$data[$k] = $v;
				}

				foreach ($data as $f => $v) {
					switch ($f) {
					    case "reference_no":
					        $row['form_no'] = $v;
					        break;
					    case "creation_date":
					        $row['updated_on'] = date("Y-m-d H:i:s", strtotime($v));
					        break;
					    case "created_by":
					        $row['updated_by'] = $v;
					        break;
					    case "date_of_entry":
					        $row['validated_on'] = date("Y-m-d H:i:s", strtotime($v));
					        break;
					    /*case "machine_id":
					        $row['laptop'] = $v;
					        break;*/
					    case "name_eng":
					        $row['name'] = $v;
					        $row['name_en'] = $v;
					        break;
					    case "father_name_eng":
					        $row['father'] = $v;
					        break;
					   case "mother_name_eng":
					       $row['mother'] = $v;
					       break;
					   case "date_of_birth":
					        $row['birth'] = date("Y-m-d", strtotime($v));
					        break;
					    case "place_of_birth":
					        $row['birth_place_other'] = "1143";
					        break;
					    case "gender":
					        $row['gender'] = strtolower($v);
					        break;
					    case "religion":
					        $row['religion'] = strtolower($v);
					        break;
						}
				}

				if($id == '')
					$row['id'] = generate_random_id();
				else
					$row['id'] = $id;

				$row['status'] = "Complete";
				$row['tag'] = "ROHINGA";
				$row['marital'] = "Unmarried";
				$row['occupation'] = "NA";
				$row['district'] = "99";
				$row['division'] = "99";
				$row['rmo'] = "1";
				$row['upozila'] = "9999";
				$row['eunion'] = "999999";
				$row['ward'] = "0";
				$row['village'] = "9999999999";
				$row['area'] = "99999999";
				$row['house'] = "NA";
				$row['postoffice'] = "NA";
				$row['postcode'] = "NA";
				$row['per_district'] = "PRESENT";
				$row['per_district'] = "99";
				$row['per_division'] = "99";
				$row['per_rmo'] = "1";
				$row['per_upozila'] = "9999";
				$row['per_eunion'] = "999999";
				$row['per_ward'] = "0";
				$row['per_village'] = "9999999999";
				$row['per_area'] = "99999999";
				$row['per_house'] = "NA";
				$row['per_postoffice'] = "NA";
				$row['per_postcode'] = "NA";
				$row['voter_area'] = "NA";
				$row['laptop'] = "ROHINGA";
				$row['birth_place_other'] = null;
				$row['stationid'] = "UKHIA_ROHINGA_CAMP";
				$row['house'] = "NA";
				$row['house'] = "NA";
				$row['house'] = "NA";
				$row['house'] = "NA";

			return $row;
		}

}

if (!function_exists('my_second_function'))
{
    function my_second_function($params)
    {
        //Your code here
    }
}

?>